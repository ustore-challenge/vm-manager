package com.ustore.vm_manager.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ustore.vm_manager.model.VirtualMachine;

public interface VirtualMachineRepository extends JpaRepository<VirtualMachine, Integer> {

}
