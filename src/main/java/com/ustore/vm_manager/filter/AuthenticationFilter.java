package com.ustore.vm_manager.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.ustore.vm_manager.exception.UnauthorizedTokenException;
import com.ustore.vm_manager.sso.SsoClient;

@Component
@Order(1)
public class AuthenticationFilter implements Filter {

	@Autowired
    private ApplicationContext applicationContext;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
	    String accessToken = httpRequest.getHeader("USER_TOKEN");
		SsoClient ssoClient = applicationContext.getBean(SsoClient.class);
		
		try {
			if(ssoClient.verifyToken(accessToken))
			{
				chain.doFilter(request, response);
			}else {
				throw new UnauthorizedTokenException();
			}
		} catch (IOException | InterruptedException | UnauthorizedTokenException e) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is not valid.");
		}
	}

}
