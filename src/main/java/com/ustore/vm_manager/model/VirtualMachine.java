package com.ustore.vm_manager.model;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class VirtualMachine {

	public static final Integer STATUS_REGISTERED = 0;
	public static final Integer STATUS_CREATION_PENDING = 1;
	public static final Integer STATUS_RUNNING = 2;
	public static final Integer STATUS_FAILURE = 3;
	public static final Integer STATUS_UPDATE_PENDING = 4;
	public static final Integer STATUS_EXCLUSION_PENDING = 5;
	public static final Integer[] STATUS_OPTIONS = {STATUS_REGISTERED,
			STATUS_CREATION_PENDING,
			STATUS_RUNNING,
			STATUS_FAILURE,
			STATUS_UPDATE_PENDING,
			STATUS_EXCLUSION_PENDING};
	
	public static final String MEMORY_1 = "256m";
	public static final String MEMORY_2 = "512m";
	public static final String MEMORY_3 = "1024m";
	public static final String[] MEMORY_OPTIONS = {MEMORY_1, MEMORY_2, MEMORY_3};
	
	public static final Float CPU_1 = 0.25F;
	public static final Float CPU_2 = 0.5F;
	public static final Float CPU_3 = 1F;
	public static final Float[] CPU_OPTIONS = {CPU_1, CPU_2, CPU_3};
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(unique = true, nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Integer status;
	
	@Column(nullable = false)
	private String memory;
	
	@Column(nullable = false)
	private String discs;
	
	@Column(nullable = false)
	private Float cpu;
	
	@Column(nullable = false)
	private Date createdAt;
	
	@Column(nullable = false)
	private Date updatedAt;

	public void setName(String name) {
		this.name = name;
		this.updatedAt = new Date();
	}

	public void setStatus(Integer status) {
		this.status = status;
		this.updatedAt = new Date();
	}

	public void setMemory(String memory) {
		this.memory = memory;
		this.updatedAt = new Date();
	}

	public void setDiscs(String discs) {
		this.discs = discs;
		this.updatedAt = new Date();
	}

	public void setCpu(Float cpu) {
		this.cpu = cpu;
		this.updatedAt = new Date();
	}

	public VirtualMachine() {
		this.createdAt = new Date();
		this.updatedAt = this.createdAt;
	}
	
	public boolean isValid() {
		
		return this.name != null
				&& !this.name.isBlank()
				&& this.status != null
				&& Arrays.stream(STATUS_OPTIONS).anyMatch(this.status::equals)
				&& this.memory != null
				&& Arrays.stream(MEMORY_OPTIONS).anyMatch(this.memory::equals)
				&& this.discs != null
				&& !this.discs.isBlank()
				&& this.cpu != null
				&& Arrays.stream(CPU_OPTIONS).anyMatch(this.cpu::equals);
	}
}
