package com.ustore.vm_manager.sso;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Component
@Data
public class SsoClient {

	@Value("${sso.authentication.verify.url}")
	private String verifyURI;
	
	public boolean verifyToken(String tk) throws IOException, InterruptedException {
		boolean verified = false;
		if (tk != null && !tk.isEmpty()) {
			String jsonToken = buildJsonString(tk);
			int result = doPostVerify(jsonToken);
			verified = result == 200;
		}
		return verified;
	}
	
	private String buildJsonString(String tk) throws JsonProcessingException {
		SsoToken token = new SsoToken(tk);
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(token);
	}
	
	public Integer doPostVerify(String jsonToken) throws IOException, InterruptedException {
		
		HttpClient client = HttpClient.newBuilder().build();
	    HttpRequest request = HttpRequest.newBuilder()
	            .header(HttpHeaders.CONTENT_TYPE, "application/json")
	    		.uri(URI.create(verifyURI))
	            .POST(BodyPublishers.ofString(jsonToken))
	            .build();

	    HttpResponse<?> response = client.send(request, BodyHandlers.discarding());
	    return response.statusCode();
	}
}
