package com.ustore.vm_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VmManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VmManagerApplication.class, args);
	}

}
