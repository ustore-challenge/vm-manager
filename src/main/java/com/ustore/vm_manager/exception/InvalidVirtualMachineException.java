package com.ustore.vm_manager.exception;

@SuppressWarnings("serial")
public class InvalidVirtualMachineException extends Exception {

	public InvalidVirtualMachineException(String errorMessage) {
	    super(errorMessage);
	}
	
	public InvalidVirtualMachineException() {
	    super();
	}
}
