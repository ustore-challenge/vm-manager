package com.ustore.vm_manager.exception;

@SuppressWarnings("serial")
public class UnauthorizedTokenException extends Exception {

	public UnauthorizedTokenException() {
		super();
	}
}
