package com.ustore.vm_manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.ustore.vm_manager.exception.InvalidVirtualMachineException;
import com.ustore.vm_manager.model.VirtualMachine;
import com.ustore.vm_manager.repo.VirtualMachineRepository;

@RestController
@RequestMapping("/vm")
public class VirtualMachineController {

	@Autowired
	private VirtualMachineRepository vmRepository;
	
	@PostMapping("/create")
	public VirtualMachine doCreate(@RequestBody VirtualMachine vm) {
		VirtualMachine newVm = null;
		try {
			newVm = registerVm(vm);
			newVm = sendToCreation(newVm);
		} catch (InvalidVirtualMachineException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		return newVm;
	}
	
	private VirtualMachine registerVm(VirtualMachine vm) throws InvalidVirtualMachineException {
		vm.setStatus(VirtualMachine.STATUS_REGISTERED);
		if(!vm.isValid()) {
			throw new InvalidVirtualMachineException();
		}
		return this.vmRepository.save(vm);
	}
	
	private VirtualMachine sendToCreation(VirtualMachine vm) throws InvalidVirtualMachineException {
		if(!vm.isValid()) {
			throw new InvalidVirtualMachineException();
		}
//		TODO Implement sending VM to worker creation queue
		vm.setStatus(VirtualMachine.STATUS_CREATION_PENDING);
		return this.vmRepository.save(vm);
	}
}
