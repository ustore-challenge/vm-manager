package com.ustore.vm_manager.util;

public class TestObjects {

	public static final String NEW_VM_1 = "{\"name\": \"my-vm\",\"memory\": \"256m\",\"discs\": \"disc-0\",\"cpu\": 0.25}";
	public static final String VM_MEMORY_NO_PREDEFINED_VALUE = "{\"name\": \"my-vm\",\"memory\": \"756m\",\"discs\": \"disc-0\",\"cpu\": 0.25}";
}
