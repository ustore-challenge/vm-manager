package com.ustore.vm_manager.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("VirtualMachine model tests")
public class VirtualMachineTest {
	
	@Test
	@DisplayName("Validate valid VM")
	public void validateVirtualMachine() throws Exception {
		VirtualMachine vm = new VirtualMachine();
		vm.setName("my_vm");
		vm.setStatus(VirtualMachine.STATUS_REGISTERED);
		vm.setMemory(VirtualMachine.MEMORY_1);
		vm.setDiscs("disc_0");
		vm.setCpu(VirtualMachine.CPU_1);
		
		assertTrue(vm.isValid());
	}
	
	@Test
	@DisplayName("Validate not predefined status VM")
	public void validateWrongStatusVm() throws Exception {
		VirtualMachine vm = new VirtualMachine();
		vm.setName("my_vm");
		vm.setStatus(10);
		vm.setMemory(VirtualMachine.MEMORY_1);
		vm.setDiscs("disc_0");
		vm.setCpu(VirtualMachine.CPU_1);
		
		assertFalse(vm.isValid());
	}
	
	@Test
	@DisplayName("Validate not predefined memory VM")
	public void validateWrongMemoryVm() throws Exception {
		VirtualMachine vm = new VirtualMachine();
		vm.setName("my_vm");
		vm.setStatus(VirtualMachine.STATUS_REGISTERED);
		vm.setMemory("1200m");
		vm.setDiscs("disc_0");
		vm.setCpu(VirtualMachine.CPU_1);
		
		assertFalse(vm.isValid());
	}
	
	@Test
	@DisplayName("Validate not predefined cpu VM")
	public void validateWrongCpuVm() throws Exception {
		VirtualMachine vm = new VirtualMachine();
		vm.setName("my_vm");
		vm.setStatus(VirtualMachine.STATUS_REGISTERED);
		vm.setMemory(VirtualMachine.MEMORY_1);
		vm.setDiscs("disc_0");
		vm.setCpu(2.0F);
		
		assertFalse(vm.isValid());
	}
}
