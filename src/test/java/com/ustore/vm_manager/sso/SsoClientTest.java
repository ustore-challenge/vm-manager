package com.ustore.vm_manager.sso;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class SsoClientTest extends Mockito {

//	@Mock
//    private SsoClient ssoClient;
		
	@Test
	public void verifyValidToken() throws Exception {
		SsoClient ssoClient = mock(SsoClient.class);
		when(ssoClient.doPostVerify("{\"token\":\"valid-token\"}")).thenReturn(200);
		when(ssoClient.verifyToken("valid-token")).thenCallRealMethod();
		
		boolean isValidToken = ssoClient.verifyToken("valid-token");
		
		assertTrue(isValidToken);
	}
	
	@Test
	public void verifyInvalidToken() throws Exception {
		SsoClient ssoClient = mock(SsoClient.class);
		when(ssoClient.doPostVerify("{\"token\":\"invalid-token\"}")).thenReturn(400);
		when(ssoClient.verifyToken("invalid-token")).thenCallRealMethod();
		
		boolean isValidToken = ssoClient.verifyToken("invalid-token");
		
		assertFalse(isValidToken);
	}
}
