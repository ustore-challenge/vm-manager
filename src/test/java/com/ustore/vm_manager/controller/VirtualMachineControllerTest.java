package com.ustore.vm_manager.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.ustore.vm_manager.model.VirtualMachine;
import com.ustore.vm_manager.sso.SsoClient;
import com.ustore.vm_manager.util.TestObjects;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class VirtualMachineControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private SsoClient ssoClient;
	
	@Test
	public void createVm() throws Exception {
		when(ssoClient.verifyToken("valid-token")).thenReturn(true);
		
		this.mockMvc.perform(post("/vm/create")
				.content(TestObjects.NEW_VM_1)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.header("USER_TOKEN", "valid-token"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name", is("my-vm")))
		.andExpect(jsonPath("$.status", is(VirtualMachine.STATUS_CREATION_PENDING)))
		.andExpect(jsonPath("$.memory", is("256m")))
		.andExpect(jsonPath("$.discs", is("disc-0")))
		.andExpect(jsonPath("$.cpu", is(0.25)));
	}
	
	@Test
	public void createWrongVm() throws Exception {
		when(ssoClient.verifyToken("valid-token")).thenReturn(true);
		
		this.mockMvc.perform(post("/vm/create")
				.content(TestObjects.VM_MEMORY_NO_PREDEFINED_VALUE)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.header("USER_TOKEN", "valid-token"))
		.andDo(print())
		.andExpect(status().isBadRequest());
	}
}
