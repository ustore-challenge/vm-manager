FROM maven:3.6.0-jdk-11 as build-image

ARG GIT_DEPLOY_USERNAME
ARG GIT_DEPLOY_TOKEN

# install git
RUN apt-get install --no-install-recommends -y git \
	ca-certificates \
    && rm -rf /var/lib/apt/lists/* \    
	&& git clone https://$GIT_DEPLOY_USERNAME:$GIT_DEPLOY_TOKEN@gitlab.com/ustore-challenge/vm-manager.git \
	&& cd vm-manager \
	&& MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true" \
	&& mvn $MAVEN_CLI_OPTS verify
	
FROM openjdk:11-slim

ENV MYSQL_HOST="localhost"
ENV MYSQL_PORT="3306"
ENV MYSQL_DB="test-db"
ENV MYSQL_USER="user"
ENV MYSQL_PW="password"
ENV SSO_VERIFY_URL="http://localhost:8081/session/verify"
COPY --from=build-image /vm-manager/target/*.jar vm-manager.jar
ENTRYPOINT ["java","-jar","/vm-manager.jar"]